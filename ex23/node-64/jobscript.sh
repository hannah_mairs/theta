#!/bin/bash
#COBALT -t 30
#COBALT -n 64

aprun -n 4096 -N 64 /home/morgan/petsc/src/ksp/ksp/examples/tutorials/ex23 -n 98304 -pc_type jacobi -ksp_type gmres -ksp_max_it 5000 -log_view
