README

theta/ex23 contains output from PETSc tutorial ex.23 with GMRES on ALCF's Theta XC40

naming convention:
node-#: number of nodes
n-#: dimension of n x n matrix

files:
gmres_rank#.txt: each line is the time for one iteration of GMRES on rank #
gmres#.txt: -log_view of ex.23 run # 

run logs and jobscripts included

theta/scripts: Python scripts for distribution fitting, graphing, etc.


----------------------------------------------------------------------------------------------


theta/ex23/pgmres contains output from PETSc tutorial ex.23 with PGMRES on ALCF's Theta XC40

node-#: number of nodes
pgmres_rank#.txt: each line is the time for one iteration of PGMRES on rank #

theta/pgmres-scripts: Python scripts for processing and plotting
