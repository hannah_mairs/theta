#
# find best distribution 
# 
# TODO:
# 	get data
#  	find best distribution
#
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as sts

# go through all scipy distributions and find the best fit
def find_best_dist(data, rank, save_path):

	# graph the data as a distribution
	y, x = np.histogram(data, bins=200, density=True)
	x = x[:-1] # x axis too long

	# distributions to test
	# some aren't working on my mac: sts.exponnorm sts.gennorm, sts.halfgennorm, sts.vonmises_line, sts.levy_stable
	distributions = [
		sts.alpha,sts.anglit,sts.arcsine,sts.beta,sts.betaprime,sts.bradford,sts.burr,sts.cauchy,sts.chi,sts.chi2,sts.cosine,
		sts.dgamma,sts.dweibull,sts.erlang,sts.expon,sts.exponweib,sts.exponpow,sts.f,sts.fatiguelife,sts.fisk,
		sts.foldcauchy,sts.foldnorm,sts.frechet_r,sts.frechet_l,sts.genlogistic,sts.genpareto,sts.genexpon,
		sts.genextreme,sts.gausshyper,sts.gamma,sts.gengamma,sts.genhalflogistic,sts.gilbrat,sts.gompertz,sts.gumbel_r,
		sts.gumbel_l,sts.halfcauchy,sts.halflogistic,sts.halfnorm,sts.hypsecant,sts.invgamma,sts.invgauss,
		sts.invweibull,sts.johnsonsb,sts.johnsonsu,sts.ksone,sts.kstwobign,sts.laplace,sts.levy,sts.levy_l,
		sts.logistic,sts.loggamma,sts.loglaplace,sts.lognorm,sts.lomax,sts.maxwell,sts.mielke,sts.nakagami,sts.ncx2,sts.ncf,
		sts.nct,sts.norm,sts.pareto,sts.pearson3,sts.powerlaw,sts.powerlognorm,sts.powernorm,sts.rdist,sts.reciprocal,
		sts.rayleigh,sts.rice,sts.recipinvgauss,sts.semicircular,sts.t,sts.triang,sts.truncexpon,sts.truncnorm,sts.tukeylambda,
		sts.uniform,sts.vonmises,sts.wald,sts.weibull_min,sts.weibull_max,sts.wrapcauchy
		]

	# save all sses to print 
	sses = []

	# keep track of who's best (assume normal distribution)
	best_distribution = sts.norm
	best_params = (0.0, 1.0)
	best_sse = np.inf

	# check each distribution
	for dist in distributions:

		# fit dist to my data
		params = dist.fit(data)

		# grab the parameters
		arg = params[:-2]
		loc = params[-2]
		scale = params[-1]

		# fit pdf and calculate SSE error
		fitted_pdf = dist.pdf(x, loc=loc, scale=scale, *arg)
		sse = np.sum(np.power(y - fitted_pdf, 2.0))

		sses.append(sse)

		# check if it's the best so far
		if best_sse > sse > 0:
			best_distribution = dist
			best_params = params
			best_sse = sse

	# print them out from best to worst
	indicies = list(np.argsort(sses)) # want smallest sse 	

	file1 = open(save_path + "dists_and_sses_rank" + str(rank) + ".txt", "w")

	for i in indicies:
		file1.write("%s\n" % distributions[i].name)
		file1.write("%s\n" % sses[i])

	# print good_distributions
	return best_distribution, best_params, best_sse

# read the file for iteration times
def get_iter_times(filename):
	times = []
	file = open(filename, "r")
	for line in file:
		times.append(float(line))
	return times

def find(pgmres_filepath, rank, save_path):

	data = get_iter_times(pgmres_filepath + str(rank) + ".txt")
	dist, params, sse = find_best_dist(data, rank, save_path)

	print "Best dist:"
	print dist

# find("../ex23/pgmres/node-64/pgmres_rank", 100, "plots/")
# find("../ex23/pgmres/node-64/pgmres_rank", 1, "plots/")









