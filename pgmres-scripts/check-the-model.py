# 
# check the model T' = \max_p \sum_k T^k_p
# 
# TODO:
# 	get the data
#
# 	fix p, calculate sum_p = \sum_k T^k_p
# 	pick biggest sum_p
#
# 	get KSPSolve time
import numpy as np
import re

# read the file for iteration times
def get_iter_times(filename):
	times = []
	file = open(filename, "r")
	for line in file:
		times.append(float(line))
	return times

# call get_iter_times for all files in a run
def get_all_iter_times(pgmres_filepath, P):

	# store the data
	data = []
	for rank in range(P):

		filename = pgmres_filepath + str(rank) + ".txt"
		data.append(get_iter_times(filename))

	# each entry of data is every iteration from one processor
	data = np.array(data)

	return data

# read the -log_view file for KSPSolve time
def get_KSP_runtimes_file(ksp_filepath):

	file = open(ksp_filepath, "r")
	for line in file:
		# get relevant lines
		if "KSPSolve" in line:
			# find first float
			time = float(re.findall("\d+\.\d+[eE][+-]\d+", line)[0])
			return time

def check_model(pgmres_filepath, K, P, ksp_filepath):

	data = get_all_iter_times(pgmres_filepath, P)

	# store \sum_k T^k_p
	sums = []
	for rank in range(P):
		sums.append(np.sum(data[rank]))

	sums = np.array(sums)

	# find max
	print np.max(sums)

	# find actual time
	print get_KSP_runtimes_file(ksp_filepath)

# check_model("../ex23/pgmres/node-64/pgmres_rank", 5000, 4096, "../ex23/pgmres/node-64/192306.output")

# well...the model doesn't work
# 1.885081
# 7.4825


