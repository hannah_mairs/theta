# 
# qqplot and probplot
# 
# TODO:
# 	get data for one rank
# 	pick distribution to fit
# 	make qq and prob plots
# 
# NOTES:
#	this file is a mess
#	fix it sometime
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as sts
import statsmodels.api as sm

# q-q plot
def q_q_plot(pgmres_filepath, rank, dist, dist_name, save_path, plot_name):

	# get the data
	data = get_iter_times(pgmres_filepath + str(rank) + ".txt")

	# qqplot wants an array
	data = np.array(data)

	# Q-Q plot goes here
	color1 = "steelblue"
	color2 = "grey"

	pp = sm.ProbPlot(data, dist=dist, fit=True)
	qq = pp.qqplot(marker='o', markerfacecolor=color1, markeredgecolor=color1)
	sm.qqline(qq.axes[0], line='45', fmt=color2)
	plt.title("Q-Q Plot with " + dist_name, fontsize=20)

	outname = save_path + plot_name + "_q_q_plot.png"
	# plt.savefig(outname)
	# plt.gcf().clear()
	plt.show()

	# can calculate parameters using other methods (MLE)
	# # fit dist to my data
	# params = dist.fit(data)

	# # grab the parameters
	# shape = params[:-2]
	# loc = params[-2]
	# scale = params[-1]
	# fig = sm.qqplot(data, dist=dist, distargs=shape, loc=loc, scale=scale, fit=False, line="45")

# probability plot (different than q-q plot, apparently)
def prob_plot(pgmres_filepath, rank, dist, dist_name, save_path, plot_name):

	# get the data
	data = get_iter_times(pgmres_filepath + str(rank) + ".txt")

	# fit dist to my data
	params = dist.fit(data)

	# prob plot goes here
	# sts.probplot(data, dist=dist.name, fit=True, plot=plt) # maybe this works too?
	sts.probplot(data, sparams=params, dist=dist.name, fit=False, plot=plt)
	plt.title("Probability Plot with " + dist_name, fontsize=20)
	# outname = save_path + plot_name + "_prob_plot.png"
	# plt.savefig(outname)
	# plt.gcf().clear()
	plt.show()

# read a file for iteration times
def get_iter_times(filename):
	times = []
	file = open(filename, "r")
	for line in file:
		times.append(float(line))
	return times

# make both plots
def make_plots(pgmres_filepath, rank, dist, dist_name, save_path, plot_name):

	q_q_plot(pgmres_filepath, rank, dist, dist_name, save_path, plot_name)
	prob_plot(pgmres_filepath, rank, dist, dist_name, save_path, plot_name)

# make the plots
make_plots("../ex23/pgmres/node-64/pgmres_rank", 100, sts.cauchy, "Cauchy", "plots/", "")
make_plots("../ex23/pgmres/node-64/pgmres_rank", 100, sts.nct, "Non-central \nStudent's T", "plots/", "")



