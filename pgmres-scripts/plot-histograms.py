# 
# plot histograms
# 
# TODO:
# 	fix p, plot histogram of T^k_p for all k (what we use for the PGMRES model)
#   fix k, plot histogram of T^k_p for all p
import re
import matplotlib.pyplot as plt
import numpy as np

# read the file for iteration times
def get_iter_times(filename):
	times = []
	file = open(filename, "r")
	for line in file:
		times.append(float(line))
	return times

# call get_iter_times for all files in a run
def get_all_iter_times(pgmres_filepath, P):

	# store the data
	data = []
	for rank in range(P):

		filename = pgmres_filepath + str(rank) + ".txt"
		data.append(get_iter_times(filename))

	# each entry of data is every iteration from one processor
	data = np.array(data)

	return data

# fix p, plot histogram
def rank_hist(pgmres_filepath, rank1, rank2, rank3, save_path):

	# don't need all of the data
	a = get_iter_times(pgmres_filepath + str(rank1) + ".txt")
	b = get_iter_times(pgmres_filepath + str(rank2) + ".txt")
	c = get_iter_times(pgmres_filepath + str(rank3) + ".txt")

	# parameters
	xmax = 0.001
	n_bins = 1000

	# plot
	plt.xlim([0, xmax])
	plt.hist(a, bins=n_bins, normed=True, color="firebrick", fill=False, histtype="step", label="Rank " + str(rank1))
	plt.hist(b, bins=n_bins, normed=True, color="steelblue", fill=False, histtype="step", label="Rank " + str(rank2))
	plt.hist(c, bins=n_bins, normed=True, color="olivedrab", fill=False, histtype="step", label="Rank " + str(rank3))

	labels = [0, 0.2, 0.4, 0.6, 0.8, 1.0]
	plt.xticks([0, 0.0002, 0.0004, 0.0006, 0.0008, 0.0010], labels)
	plt.title("Pipelined Krylov density on 4096 processors", fontsize=20)
	plt.xlabel("Milliseconds", fontsize=20)
	plt.ylabel("Density", fontsize=20)
	plt.legend(loc="upper right", fontsize=20)

	plt.savefig(save_path + "rank_hist.png")
	plt.gcf().clear()
	# plt.show()

# fix k, plot histogram
def iter_hist(pgmres_filepath, P, iter1, iter2, iter3, save_path):

	# get data
	data = get_all_iter_times(pgmres_filepath, P)
	data = np.transpose(data)

	a = data[iter1]
	b = data[iter2]
	c = data[iter3]

	# parameters
	xmin = 0.00018
	xmax = 0.00045
	n_bins = 20

	# plot
	plt.xlim([xmin, xmax])
	plt.hist(a, bins=n_bins, normed=True, color="firebrick", fill=False, histtype="step", label="Iteration " + str(iter1))
	plt.hist(b, bins=n_bins, normed=True, color="steelblue", fill=False, histtype="step", label="Iteration " + str(iter2))
	# plt.hist(c, bins=n_bins, normed=True, color="olivedrab", fill=False, histtype="step", label="Iteration " + str(iter3))

	labels = [0.2, 0.25, 0.3, 0.35, 0.4, 0.45]
	plt.xticks([0.0002, 0.00025, 0.0003, 0.00035, 0.0004, 0.00045], labels)
	plt.title("Pipelined Krylov density on 4096 processors", fontsize=20)
	plt.xlabel("Milliseconds", fontsize=20)
	plt.ylabel("Density", fontsize=20)
	plt.legend(loc="upper right", fontsize=20)

	plt.savefig(save_path + "iter_hist.png")
	plt.gcf().clear()
	# plt.show()


# make plots
# rank_hist("../ex23/pgmres/node-64/pgmres_rank", 0, 1, 2048, "plots/")
# iter_hist("../ex23/pgmres/node-64/pgmres_rank", 4096, 100, 2500, 2500, "plots/")











