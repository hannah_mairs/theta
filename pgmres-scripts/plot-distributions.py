# 
# plot distributions with data
# 
# NOTES:
#	data for one rank
#	includes filling the pipeline
# 
# TODO:
# 	get the data
# 	fit a distribution to the data
# 	plot distribution
# 	plot histogram of data
#
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as sts

# plots fitted distribution for data from one rank of PGMRES
def plot_dist(pgmres_filepath, rank, dist, name_of_dist, save_path):

	# get the data
	data = get_iter_times(pgmres_filepath + str(rank) + ".txt")

	# fit dist to my data
	params = dist.fit(data)

	# grab the parameters
	arg = params[:-2]
	loc = params[-2]
	scale = params[-1]

	y, x = np.histogram(data, bins=2000, density=True)
	x = x[:-1] # x axis too long

	fitted_pdf = dist.pdf(x, loc=loc, scale=scale, *arg)

	# parameters
	xmax = 0.001
	n_bins = 1000

	# plot
	plt.xlim([0, xmax])

	plt.hist(data, bins=n_bins, fill=False, histtype="step", color="firebrick", normed=True, label="Rank " + str(rank))
	plt.plot(x, fitted_pdf, color="steelblue", label=name_of_dist)

	labels = [0, 0.2, 0.4, 0.6, 0.8, 1.0]
	plt.xticks([0, 0.0002, 0.0004, 0.0006, 0.0008, 0.0010], labels)

	plt.title("Pipelined Krylov with fitted distribution", fontsize=20)
	plt.xlabel("Milliseconds", fontsize=20)
	plt.ylabel("Density", fontsize=20)
	plt.legend(loc="upper right", fontsize=20)

	outname = save_path + "rank_hist_" + name_of_dist + "_rank" + str(rank) + ".png"
	plt.savefig(outname)
	plt.gcf().clear()
	# plt.show()

# read a file for iteration times
def get_iter_times(filename):
	times = []
	file = open(filename, "r")
	for line in file:
		times.append(float(line))
	return times

# make the plots
# plot_dist("../ex23/pgmres/node-64/pgmres_rank", 1, sts.gilbrat, "Gilbrat", "plots/")
# plot_dist("../ex23/pgmres/node-64/pgmres_rank", 1, sts.johnsonsb, "Johnson SB", "plots/")
# plot_dist("../ex23/pgmres/node-64/pgmres_rank", 1, sts.lognorm, "Log-normal", "plots/")
# plot_dist("../ex23/pgmres/node-64/pgmres_rank", 100, sts.cauchy, "Cauchy", "plots/")
# plot_dist("../ex23/pgmres/node-64/pgmres_rank", 100, sts.nct, "Non-central \nStudent's T", "plots/")




