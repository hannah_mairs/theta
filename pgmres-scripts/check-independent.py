# 
# bar graph for independent in p
# 
# TODO:
# 	get the data for one iteration
#  	plot bar graph of time for each rank
# 
import matplotlib.pyplot as plt
import numpy as np

# read a file for iteration times
def get_iter_times(filename):
	times = []
	file = open(filename, "r")
	for line in file:
		times.append(float(line))
	return times

# read all the files of one run for iteration times
def get_all_iter_times(path_to_data, P, K):

	data = []
	for rank in range(P):

		filename = path_to_data + str(rank) + ".txt"
		data.append(get_iter_times(filename))

	# each entry of data is every iteration from one processor
	data = np.array(data)
	# now make each entry one iteration from every processor
	data_transpose = np.transpose(data)

	return data_transpose

# bar graph of iteration times for fixed iteration k
def independent_in_p(path_to_data, iteration, P, K, path_to_plots):

	data = get_all_iter_times(path_to_data, P, K)

	# iterations to plot
	a = data[iteration]
	x = np.arange(len(a))

	xmin = 0
	xmax = 2000
	plt.xlim([xmin, xmax])
	ymin = 0.00032
	ymax = .00046
	# plt.ylim([ymin, ymax])

	plt.bar(x, a, width=.000001, color="grey", edgecolor="grey", label="Iteration " + str(iteration))
	
	# ylabels = [0.3, 0.35, 0.4, 0.45, 0.5]
	# plt.yticks([0.0003, 0.00035, 0.0004, 0.00045, 0.0005], ylabels)
	plt.title("Krylov iteration time on 4096 processors", fontsize=20)
	plt.xlabel("Processor number", fontsize=20)
	plt.ylabel("Seconds", fontsize=20)
	plt.legend(loc="upper right", fontsize=20)
	plt.savefig(path_to_plots + "independent_in_p_" + str(iteration) + ".pdf")
	plt.gcf().clear()
	# plt.show()

# run the plots
independent_in_p("../ex23/pgmres/node-64/pgmres_rank", 100, 4096, 5000, "plots/")








