# 
# use stochastic model E[T'] = K \mu
# 
# TODO:
# 	get data
# 	pick distribution
# 	find mean of distribution
#	calculate E[T']
#	compare to T' from KSPSolve
# 
# NOTES:
# 	NOT DONE
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as sts
import re

# read the file for iteration times
def get_iter_times(filename):
	times = []
	file = open(filename, "r")
	for line in file:
		times.append(float(line))
	return times

# read the -log_view file for KSPSolve time
def get_KSP_runtimes_file(ksp_filepath):

	file = open(ksp_filepath, "r")
	for line in file:
		# get relevant lines
		if "KSPSolve" in line:
			# find first float
			time = float(re.findall("\d+\.\d+[eE][+-]\d+", line)[0])
			return time

# use the stochastic model to find expected runtime
def stochastic_model(pgmres_filepath, rank, dist, ksp_filepath):

	# get the data
	data = get_iter_times(pgmres_filepath + str(rank) + ".txt")

	print np.mean(data)
	print 5000*np.mean(data)

	# fit dist to my data
	params = dist.fit(data)

	print dist.fit(data).mean()

	# grab the parameters
	arg = params[:-2]
	loc = params[-2]
	scale = params[-1]

	y, x = np.histogram(data, bins=2000, density=True)
	x = x[:-1] # x axis too long

	fitted_pdf = dist.pdf(x, loc=loc, scale=scale, *arg)

	print np.mean(fitted_pdf)

stochastic_model("../ex23/pgmres/node-64/pgmres_rank", 100, sts.cauchy, "")










