# 
# nice pictures after stationary in time assumption failed
# 
# 
# 

import re # for regular expression
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as sts

# read a file for iteration times
def get_iter_times(filename):
	times = []
	file = open(filename, "r")
	for line in file:
		times.append(float(line))
	return times

# read all the files of one run for iteration times
def get_all_iter_times(path_to_data, P, K):

	data = []
	for rank in range(P):

		filename = path_to_data + str(rank) + ".txt"
		data.append(get_iter_times(filename))

	# each entry of data is every iteration from one processor
	data = np.array(data)
	# now make each entry one iteration from every processor
	data_transpose = np.transpose(data)

	return data_transpose

# plot a histogram of runtime from every processor for a fixed iteration
def fifteen_mod_30(path_to_data, P, K, path_to_plots):

	data = get_all_iter_times(path_to_data, P, K)

	# iterations to plot
	iter1 = 45
	a = data[iter1]

	# parameters
	xmin = 0.00015
	xmax = 0.00045
	n_bins = 15

	# plot
	plt.xlim([xmin, xmax])
	plt.hist(a, bins=n_bins, normed=True, color="firebrick", fill=False, histtype="step", label="Iteration " + str(iter1))

	labels = [0.15, 0.2, 0.3, 0.4, 0.45]
	plt.xticks([0.00015, 0.0002, 0.0003, 0.0004, 0.00045], labels)
	plt.title("Krylov iteration density on 4096 processors", fontsize=20)
	plt.xlabel("Milliseconds", fontsize=20)
	plt.ylabel("Density", fontsize=20)
	plt.legend(loc="upper right", fontsize=20)
	plt.savefig(path_to_plots + "fifteen_mod_30.png")
	plt.gcf().clear()
	# plt.show()

# are all 15 mod 30s the same?
def plot_all_fifteen_mod_30s(path_to_data, P, K):

	data = get_all_iter_times(path_to_data, P, K)

	# iterations to plot
	iter1 = 45
	iter2 = 75

	a = data[iter1]
	

	# parameters
	xmin = 0.00015
	xmax = 0.00045
	n_bins = 15

	while iter2 < 5000:

		b = data[iter2]

		# plot
		plt.xlim([xmin, xmax])
		plt.hist(a, bins=n_bins, normed=True, color="firebrick", fill=False, histtype="step", label="Iteration " + str(iter1))
		plt.hist(b, bins=n_bins, normed=True, color="steelblue", fill=False, histtype="step", label="Iteration " + str(iter2))

		labels = [0.15, 0.2, 0.3, 0.4, 0.45]
		plt.xticks([0.00015, 0.0002, 0.0003, 0.0004, 0.00045], labels)
		plt.title("Krylov iteration density on 4096 processors", fontsize=20)
		plt.xlabel("Milliseconds", fontsize=20)
		plt.ylabel("Density", fontsize=20)
		plt.legend(loc="upper right", fontsize=20)
		plt.show()

		iter2 += 30


def all_k_p(path_to_data, P, K, path_to_plots):

	# want all data in one list
	data = []
	for rank in range(P):

		filename = path_to_data + str(rank) + ".txt"
		data.append(get_iter_times(filename))

	# get data out of nested list
	data = data[0]

	# parameters
	xmax = 0.0008
	n_bins = 50

	# plot
	plt.xlim([0, xmax])
	plt.hist(data, bins=n_bins, normed=True, color="firebrick", fill=False, histtype="step", label="Bulk statistics")

	labels = [0, 0.2, 0.4, 0.6, 0.8]
	plt.xticks([0, 0.0002, 0.0004, 0.0006, 0.0008], labels)
	plt.title("Krylov iteration density on 4096 processors", fontsize=20)
	plt.xlabel("Milliseconds", fontsize=20)
	plt.ylabel("Density", fontsize=20)
	plt.legend(loc="upper right", fontsize=20)
	plt.savefig(path_to_plots + "all_k_p.png")
	plt.gcf().clear()
	# plt.show()

# run the plots
fifteen_mod_30("../ex23/node-64/gmres_rank", 4096, 5000, "../ex23/node-64/plots/")
all_k_p("../ex23/node-64/gmres_rank", 4096, 5000, "../ex23/node-64/plots/")
# plot_all_fifteen_mod_30s("../ex23/node-64/gmres_rank", 4096, 5000)

