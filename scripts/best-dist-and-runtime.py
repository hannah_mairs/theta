#
# find best distribution to model
#
# find_best_dist() 
#    fitted distributions from best to worst
#    calculate expected runtime from dist
#
# do all of this for representative iteration (I chose 45)
# do all of this for "bulk statistics"

import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as sts
from scipy.integrate import quad

# read a file for iteration times
def get_iter_times(filename):
	times = []
	file = open(filename, "r")
	for line in file:
		times.append(float(line))
	return times

# read all the files of one run for iteration times
def get_all_iter_times(path_to_data, P, K):

	data = []
	for rank in range(P):

		filename = path_to_data + str(rank) + ".txt"
		data.append(get_iter_times(filename))

	# each entry of data is every iteration from one processor
	data = np.array(data)
	# now make each entry one iteration from every processor
	data_transpose = np.transpose(data)

	return data_transpose

# go through all scipy distributions and find the best fit
# 1. go through and find best fits, print them out
def find_best_dist(data, path_to_file, P, K, pipe):

	# graph the data as a distribution
	y, x = np.histogram(data, bins=2000, density=True)
	x = x[:-1] # x axis too long

	# distributions to test
	# some aren't working on my mac: sts.exponnorm sts.gennorm, sts.halfgennorm, sts.vonmises_line, sts.levy_stable
	distributions = [
		sts.alpha,sts.anglit,sts.arcsine,sts.beta,sts.betaprime,sts.bradford,sts.burr,sts.cauchy,sts.chi,sts.chi2,sts.cosine,
		sts.dgamma,sts.dweibull,sts.erlang,sts.expon,sts.exponweib,sts.exponpow,sts.f,sts.fatiguelife,sts.fisk,
		sts.foldcauchy,sts.foldnorm,sts.frechet_r,sts.frechet_l,sts.genlogistic,sts.genpareto,sts.genexpon,
		sts.genextreme,sts.gausshyper,sts.gamma,sts.gengamma,sts.genhalflogistic,sts.gilbrat,sts.gompertz,sts.gumbel_r,
		sts.gumbel_l,sts.halfcauchy,sts.halflogistic,sts.halfnorm,sts.hypsecant,sts.invgamma,sts.invgauss,
		sts.invweibull,sts.johnsonsb,sts.johnsonsu,sts.ksone,sts.kstwobign,sts.laplace,sts.levy,sts.levy_l,
		sts.logistic,sts.loggamma,sts.loglaplace,sts.lognorm,sts.lomax,sts.maxwell,sts.mielke,sts.nakagami,sts.ncx2,sts.ncf,
		sts.nct,sts.norm,sts.pareto,sts.pearson3,sts.powerlaw,sts.powerlognorm,sts.powernorm,sts.rdist,sts.reciprocal,
		sts.rayleigh,sts.rice,sts.recipinvgauss,sts.semicircular,sts.t,sts.triang,sts.truncexpon,sts.truncnorm,sts.tukeylambda,
		sts.uniform,sts.vonmises,sts.wald,sts.weibull_min,sts.weibull_max,sts.wrapcauchy
		]

	# keep track of who's best (assume normal distribution)
	best_distribution = sts.norm
	best_params = (0.0, 1.0)
	best_sse = np.inf

	# keep track of all sse's and runtimes from the model
	sses = []
	runtime_from_model = []

	# check each distribution
	for dist in distributions:

		# fit dist to my data
		params = dist.fit(data)

		# grab the parameters
		arg = params[:-2]
		loc = params[-2]
		scale = params[-1]

		# fit pdf and calculate SSE error
		fitted_pdf = dist.pdf(x, loc=loc, scale=scale, *arg)
		sse = np.sum(np.power(y - fitted_pdf, 2.0))
		sses.append(sse)

		# check if it's the best so far
		if best_sse > sse > 0:
			best_distribution = dist
			best_params = params
			best_sse = sse

		# then calculate the expected runtime from distribution
		def calculate_runtime_from_model(P, K, pipe):

			def integral(x):

				if(not pipe):
					return K*P*x*(dist.cdf(x, loc=loc, scale=scale, *arg)**(P-1))*dist.pdf(x, loc=loc, scale=scale, *arg)
				else:
					return K*x*dist.pdf(x, loc=loc, scale=scale, *arg)

			def runtime():
				ma = max(data)
				mi = min(data)

				expected, error = quad(integral, mi, ma)
				# expected, error = quad(integral, -np.inf, np.inf)
				# expected, error = quad(integral, 0, np.inf)

				return expected, error

			return runtime()[0]

		runtime_from_model.append(calculate_runtime_from_model(P, K, False))

	# print them out from best to worst
	indicies = list(np.argsort(sses)) # want smallest sse 	

	file1 = open(path_to_file + "distributions_sses.txt", "w")
	file2 = open(path_to_file + "caculated_runtimes.txt", "w")

	for i in indicies:
		file1.write("%s\n" % distributions[i].name)
		file1.write("%s\n" % sses[i])
		file2.write("%s\n" % runtime_from_model[i])

	return best_distribution, best_params, best_sse

# get my data for iteration 45
iter_num = 45	
runtimes = get_all_iter_times("../ex23/node-64/gmres_rank", 4096, 5000)
# find best distribution
best_distribution, best_params, best_sse = find_best_dist(runtimes[iter_num], "../ex23/node-64/plots/representative_", 4096, 5000, False)

# get my data for bulk statistics
P = 4096
runtimes = []
for rank in range(P):

	filename = "../ex23/node-64/gmres_rank" + str(rank) + ".txt"
	runtimes.append(get_iter_times(filename))

# get data out of nested list
runtimes = runtimes[0]
# find best distribution
best_distribution, best_params, best_sse = find_best_dist(runtimes, "../ex23/node-64/plots/bulk_", 4096, 5000, False)






