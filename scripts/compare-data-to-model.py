# 
# compare the data from a run of ex23 to the Krylov model: \sum_k \max_p T^k_p
# 	k: iteration number
# 	p: process number/rank
#	T^k_p: time for iteration k of process p
#
# cross your fingers that model time matches time from log_view file (otherwise something is wrong)
# 
# 
# 

import re # for regular expression
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as sts

# read the file for iteration time
def get_iter_times(filename):
	times = []
	file = open(filename, "r")
	for line in file:
		times.append(float(line))
	return times

# read the -log_view file for KSPSolve time
def get_KSP_runtimes_file(filename):
	file = open(filename, "r")
	for line in file:
		# get relevant lines
		if "KSPSolve" in line:
			# find first float
			time = float(re.findall("\d+\.\d+[eE][+-]\d+", line)[0])
			return time

# compare max time for each iteration to actual KSPSolve time
def comparing_collected_times_to_kspsolve(path_to_data, P, K, theta_output_filename):

	data = []
	for rank in range(P):

		filename = path_to_data + str(rank) + ".txt"
		data.append(get_iter_times(filename))

	# each entry of data is every iteration from one processor
	data = np.array(data)
	# now make each entry one iteration from every processor
	data_transpose = np.transpose(data)

	# find the max of each iteration
	maxes = []
	for iteration in range(K):
		maxes.append(np.max(data_transpose[iteration]))

	maxes = np.array(maxes)
	print np.sum(maxes)

	# actual runtime
	print get_KSP_runtimes_file(theta_output_filename)

comparing_collected_times_to_kspsolve("../ex23/node-64/gmres_rank", 4096, 5000, "../ex23/node-64/129727.output")

# output:
# 1.643401
# 1.5136



	












