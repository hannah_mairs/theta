
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as sts
from scipy.integrate import quad
import statsmodels.api as sm

# read a file for iteration times
def get_iter_times(filename):
	times = []
	file = open(filename, "r")
	for line in file:
		times.append(float(line))
	return times

# read all the files of one run for iteration times
def get_all_iter_times(path_to_data, P, K):

	data = []
	for rank in range(P):

		filename = path_to_data + str(rank) + ".txt"
		data.append(get_iter_times(filename))

	# each entry of data is every iteration from one processor
	data = np.array(data)
	# now make each entry one iteration from every processor
	data_transpose = np.transpose(data)

	return data_transpose

def lots_of_dists(path_to_plots, plot_name):

	# get my data for bulk statistics
	P = 4096
	runtimes = []
	for rank in range(P):

		filename = "../ex23/node-64/gmres_rank" + str(rank) + ".txt"
		runtimes.append(get_iter_times(filename))

	# get data out of nested list
	runtimes = runtimes[0]

	# fit dist to my data
	params1 = sts.nct.fit(data)
	params2 = sts.lognorm.fit(data)

	# grab the parameters
	arg1 = params1[:-2]
	loc1 = params1[-2]
	scale1 = params1[-1]

	arg2 = params2[:-2]
	loc2 = params2[-2]
	scale2 = params2[-1]

	y, x = np.histogram(data, bins=2000, density=True)
	x = x[:-1] # x axis too long

	fitted_pdf1 = sts.nct.pdf(x, loc=loc1, scale=scale1, *arg1)
	fitted_pdf2 = sts.lognorm.pdf(x, loc=loc2, scale=scale2, *arg2)

	# parameters
	xmax = 0.0008
	n_bins = 50

	# plot
	plt.xlim([0, xmax])

	plt.hist(data, bins=n_bins, fill=False, histtype="step", color="firebrick", normed=True, label="Bulk statistics")
	plt.plot(x, fitted_pdf1, color="steelblue", label="Non-central \nStudent's T")
	plt.plot(x, fitted_pdf2, color="grey", label="Log-normal")

	labels = [0, 0.2, 0.4, 0.6, 0.8]
	plt.xticks([0, 0.0002, 0.0004, 0.0006, 0.0008], labels)

	plt.title("GMRES iterations with fitted distributions", fontsize=20)
	plt.xlabel("Milliseconds", fontsize=20)
	plt.ylabel("Density", fontsize=20)
	plt.legend(loc="upper right", fontsize=20)

	outname = path_to_plots + plot_name + ".png"
	plt.savefig(outname)
	plt.gcf().clear()
	# plt.show()

# lots_of_dists("../ex23/node-64/plots/", "bulk_nct_lognorm")

def lots_and_lots_of_dists(path_to_plots, plot_name):

	# get my data for bulk statistics
	P = 4096
	runtimes = []
	for rank in range(P):

		filename = "../ex23/node-64/gmres_rank" + str(rank) + ".txt"
		runtimes.append(get_iter_times(filename))

	# get data out of nested list
	data = runtimes[0]

	y, x = np.histogram(data, bins=2000, density=True)
	x = x[:-1] # x axis too long

	dists = [sts.invgamma, sts.gumbel_r, sts.invgauss, sts.genlogistic,
			 sts.johnsonsb,  sts.powerlognorm, sts.lognorm, sts.nct] # sts.laplace, sts.dweibull, sts.genextreme,
	fitted_pdfs = []

	for dist in dists:
		params = dist.fit(data)

		# grab the parameters
		arg = params[:-2]
		loc = params[-2]
		scale = params[-1]

		fitted_pdf = dist.pdf(x, loc=loc, scale=scale, *arg)
		fitted_pdfs.append(fitted_pdf)

	# parameters
	xmax = 0.0008
	n_bins = 50

	# plot
	plt.xlim([0, xmax])
	colors = ["steelblue", "grey", "steelblue", "grey", "steelblue", "grey", "steelblue", "grey"]

	plt.hist(data, bins=n_bins, fill=False, histtype="step", color="firebrick", normed=True, label="Bulk statistics")

	for fitted_pdf, color in zip(fitted_pdfs, colors):
		plt.plot(x, fitted_pdf, color=color)#, label="Non-central \nStudent's T")


	labels = [0, 0.2, 0.4, 0.6, 0.8]
	plt.xticks([0, 0.0002, 0.0004, 0.0006, 0.0008], labels)

	plt.title("GMRES iterations with fitted distributions", fontsize=20)
	plt.xlabel("Milliseconds", fontsize=20)
	plt.ylabel("Density", fontsize=20)
	plt.legend(loc="upper right", fontsize=20)

	outname = path_to_plots + plot_name + ".png"
	plt.savefig(outname)
	plt.gcf().clear()
	# plt.show()

lots_and_lots_of_dists("../ex23/node-64/plots/", "bulk_dist_fits")


# plot a histogram of runtime from every processor for a fixed iteration
def iteration_hists(path_to_data, P, K, path_to_plots):

	data = get_all_iter_times(path_to_data, P, K)

	# iterations to plot
	iter1, iter2, iter3 = 45, 915, 1095

	a = data[iter1]
	b = data[iter2]
	c = data[iter3]

	# parameters
	xmin = 0.0002
	xmax = 0.00045
	n_bins = 10

	# plot
	plt.xlim([xmin, xmax])
	plt.hist(a, bins=n_bins, normed=True, color="firebrick", fill=False, histtype="step", label="Iteration " + str(iter1))
	plt.hist(b, bins=n_bins, normed=True, color="steelblue", fill=False, histtype="step", label="Iteration " + str(iter2))
	plt.hist(c, bins=n_bins, normed=True, color="grey", fill=False, histtype="step", label="Iteration " + str(iter3))

	labels = [0.2, 0.25, 0.3, 0.35, 0.4, 0.45]
	plt.xticks([0.0002, 0.00025, 0.0003, 0.00035, 0.0004, 0.00045], labels)
	plt.title("Krylov iteration density on 4096 processors", fontsize=20)
	plt.xlabel("Milliseconds", fontsize=20)
	plt.ylabel("Density", fontsize=20)
	plt.legend(loc="upper right", fontsize=20)
	plt.savefig(path_to_plots + "iteration_hist.png")
	plt.gcf().clear()
	# plt.show()

iteration_hists("../ex23/node-64/gmres_rank", 4096, 5000, "../ex23/node-64/plots/")


