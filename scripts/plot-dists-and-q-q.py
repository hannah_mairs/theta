# 
# probplot
# qqplot
# distribution fitting plots
# 
# 
# 
# 
# 


import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as sts
from scipy.integrate import quad
import statsmodels.api as sm

# read a file for iteration times
def get_iter_times(filename):
	times = []
	file = open(filename, "r")
	for line in file:
		times.append(float(line))
	return times

# read all the files of one run for iteration times
def get_all_iter_times(path_to_data, P, K):

	data = []
	for rank in range(P):

		filename = path_to_data + str(rank) + ".txt"
		data.append(get_iter_times(filename))

	# each entry of data is every iteration from one processor
	data = np.array(data)
	# now make each entry one iteration from every processor
	data_transpose = np.transpose(data)

	return data_transpose

# look at probability plot (different than q-q plot, apparently)
def prob_plot(data, dist, name, path_to_plots, plot_name):

	# fit dist to my data
	params = dist.fit(data)

	# prob plot goes here
	# sts.probplot(data, dist=dist.name, fit=True, plot=plt) # maybe this works too?
	sts.probplot(data, sparams=params, dist=dist.name, fit=False, plot=plt)
	plt.title("Probability Plot with " + name, fontsize=20)
	# outname = path_to_plots + plot_name + "_prob_plot.png"
	# plt.savefig(outname)
	# plt.gcf().clear()
	plt.show()

# look at q-q plot
def q_q_plot(data, dist, name, path_to_plots, plot_name):

	# qqplot wants an array
	data = np.array(data)

	# Q-Q plot goes here
	color1 = "steelblue"
	# color1 = "firebrick"
	color2 = "grey"

	pp = sm.ProbPlot(data, dist=dist, fit=True)
	qq = pp.qqplot(marker='o', markerfacecolor=color1, markeredgecolor=color1)
	sm.qqline(qq.axes[0], line='45', fmt=color2)
	plt.title("Q-Q Plot with " + name, fontsize=20)

	outname = path_to_plots + plot_name + "_q_q_plot.png"
	plt.savefig(outname)
	plt.gcf().clear()
	# plt.show()

	# can calculate parameters using other methods (MLE)
	# # fit dist to my data
	# params = dist.fit(data)

	# # grab the parameters
	# shape = params[:-2]
	# loc = params[-2]
	# scale = params[-1]
	# fig = sm.qqplot(data, dist=dist, distargs=shape, loc=loc, scale=scale, fit=False, line="45")

def plot_dist(data, dist, name_of_dist, path_to_plots, plot_name):

	# fit dist to my data
	params = dist.fit(data)

	# grab the parameters
	arg = params[:-2]
	loc = params[-2]
	scale = params[-1]

	y, x = np.histogram(data, bins=2000, density=True)
	x = x[:-1] # x axis too long

	fitted_pdf = dist.pdf(x, loc=loc, scale=scale, *arg)

	# parameters
	xmin = 0.0002
	xmax = 0.0004
	n_bins = 15

	# parameters
	# xmax = 0.0008
	# n_bins = 50

	# plot
	plt.xlim([xmin, xmax])
	# plt.xlim([0, xmax])

	plt.hist(data, bins=n_bins, fill=False, histtype="step", color="firebrick", normed=True, label="Iteration 45")
	# plt.hist(data, bins=n_bins, fill=False, histtype="step", color="firebrick", normed=True, label="Bulk statistics")
	plt.plot(x, fitted_pdf, color="steelblue", label=name_of_dist)

	labels = [0.2, 0.25, 0.3, 0.35, 0.4]
	plt.xticks([0.0002, 0.00025, 0.0003, 0.00035, 0.0004], labels)
	# labels = [0, 0.2, 0.4, 0.6, 0.8]
	# plt.xticks([0, 0.0002, 0.0004, 0.0006, 0.0008], labels)

	plt.title("GMRES iterations with fitted distribution", fontsize=20)
	plt.xlabel("Milliseconds", fontsize=20)
	plt.ylabel("Density", fontsize=20)
	plt.legend(loc="upper right", fontsize=20)

	outname = path_to_plots + plot_name + ".png"
	plt.savefig(outname)
	plt.gcf().clear()
	# plt.show()

# get my data for iteration 45
# iter_num = 45	
# runtimes = get_all_iter_times("../ex23/node-64/gmres_rank", 4096, 5000)

# make plots
# prob_plot(runtimes[iter_num], sts.invgamma, "Inverse Gamma", "../ex23/node-64/plots/", "iter45_invgamma")
# prob_plot(runtimes[iter_num], sts.gamma, "Gamma", "../ex23/node-64/plots/", "iter45_gamma")
# prob_plot(runtimes[iter_num], sts.lognorm, "Log-normal", "../ex23/node-64/plots/", "iter45_lognorm")

# q_q_plot(runtimes[iter_num], sts.invgamma, "Inverse Gamma", "../ex23/node-64/plots/", "iter45_invgamma")
# q_q_plot(runtimes[iter_num], sts.gamma, "Gamma", "../ex23/node-64/plots/", "iter45_gamma")
# q_q_plot(runtimes[iter_num], sts.lognorm, "Log-normal", "../ex23/node-64/plots/", "iter45_lognorm")

# plot_dist(runtimes[iter_num], sts.invgamma, "Inverse Gamma", "../ex23/node-64/plots/", "iter45_invgamma")
# plot_dist(runtimes[iter_num], sts.gamma, "Gamma", "../ex23/node-64/plots/", "iter45_gamma")
# plot_dist(runtimes[iter_num], sts.lognorm, "Log-normal", "../ex23/node-64/plots/", "iter45_lognorm")


# get my data for bulk statistics
P = 4096
runtimes = []
for rank in range(P):

	filename = "../ex23/node-64/gmres_rank" + str(rank) + ".txt"
	runtimes.append(get_iter_times(filename))

# get data out of nested list
runtimes = runtimes[0]

# make plots
# prob_plot(runtimes, sts.nct, "Non-central \nStudent's T", "../ex23/node-64/plots/", "bulk_nct")
# prob_plot(runtimes, sts.cauchy, "Cauchy", "../ex23/node-64/plots/", "bulk_cauchy")
# prob_plot(runtimes, sts.lognorm, "Log-normal", "../ex23/node-64/plots/", "bulk_lognorm")

q_q_plot(runtimes, sts.nct, "Non-central Student's T", "../ex23/node-64/plots/", "bulk_nct")
q_q_plot(runtimes, sts.gamma, "Gamma", "../ex23/node-64/plots/", "bulk_gamma")
q_q_plot(runtimes, sts.lognorm, "Log-normal", "../ex23/node-64/plots/", "bulk_lognorm")

# plot_dist(runtimes, sts.nct, "Non-central \nStudent's T", "../ex23/node-64/plots/", "bulk_nct")
# plot_dist(runtimes, sts.cauchy, "Cauchy", "../ex23/node-64/plots/", "bulk_cauchy")
# plot_dist(runtimes, sts.lognorm, "Log-normal", "../ex23/node-64/plots/", "bulk_lognorm")


