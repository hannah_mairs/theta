#
#
#
# following advice from Vivak
#
#
# fit data to gamma distribution
# look at Q-Q plot of data and "see if it looks alright"

import numpy as np
import scipy.stats as sts
import matplotlib.pyplot as plt
import statsmodels.api as sm
import scipy.special as sp
from scipy.integrate import quad

# read a file for iteration times
def get_iter_times(filename):
	times = []
	file = open(filename, "r")
	for line in file:
		times.append(float(line))
	return times

# use MLE to fit data to gamma distribution
def fit_gamma_MLE(data):

	# MLE forumlas here
	loc = min(data)
	data = data - loc + 0.0000001 # taking log(0) otherwise
	N = len(data)

	# estimation of MLE parameters
	s = np.log((1.0/N)*np.sum(data)) - (1.0/N)*np.sum(np.log(data))
	k = (3 - s + np.sqrt((s - 3)**2 + 24*s))/(12*s)
	theta = (1.0/(k*N))*np.sum(data)

	shape = k
	scale = theta

	return shape, loc, scale

# use scipy to fit data to gamma
def fit_gamma_scipy(data):

	shape, loc, scale = sts.gamma.fit(data)

	return shape, loc, scale

# use method of moments to fit data to gamma
def fit_gamma_moments(data):

	loc = min(data)
	data = data - loc

	m = np.mean(data)
	s = np.std(data)

	shape = (m/s)**2
	scale = (s**2)/m

	return shape, loc, scale

# look at probability plot (different than q-q plot, apparently)
def prob_plot(data, params, output_prefix_name):

	# prob plot goes here
	sts.probplot(data, sparams=params, dist="gamma", fit=False, plot=plt)
	# plt.savefig(output_prefix_name + "_prob_plot.png")
	# plt.gcf().clear()
	plt.show()

# look at q-q plot
def q_q_plot(data, params, output_prefix_name):

	# grab the parameters
	shape = params[:-2]
	loc = params[-2]
	scale = params[-1]

	# Q-Q plot goes here
	fig = sm.qqplot(data, dist=sts.gamma, distargs=shape, loc=loc, scale=scale, fit=False, line="45")
	plt.title("Q-Q Plot with gamma distribution", fontsize=20)
	# plt.savefig(output_prefix_name + "_q_q_plot.png")
	# plt.gcf().clear()
	plt.show()
	return


# look at data and MLE gamma
def plot_data_and_gamma(data, params, output_prefix_name, P):

	shape = params[0]
	loc = params[1]
	scale = params[2]

	y, x = np.histogram(data, bins=2000, density=True)
	x = x[:-1] # x axis too long

	fitted_pdf = sts.gamma.pdf(x, shape, loc, scale)

	# xmax = 0.001
	# plt.xlim([0, xmax])
	plt.hist(data, bins=75, fill=False, histtype="step", color="grey", normed=True, label= str(P) + " processors")
	plt.plot(x, fitted_pdf, color="firebrick", label="Gamma")
	# labels = [0, 0.2, 0.4, 0.6, 0.8, 1.0]
	# plt.xticks([0, 0.0002, 0.0004, 0.0006, 0.0008, 0.0010], labels)
	plt.title("Probability distribution fit to Krylov runtimes", fontsize=20)
	# plt.xlabel("Milliseconds", fontsize=20)
	plt.xlabel("Seconds", fontsize=20)
	plt.ylabel("Density", fontsize=20)
	plt.legend(loc="upper right", fontsize=20)
	# plt.savefig(output_prefix_name + "_data_and_gamma.png")
	# plt.gcf().clear()
	plt.show()

# calculate the expected runtime :(
def calc_expected_runtime(params, K, P):

	# grab the parameters
	shape = params[0]
	loc = params[1]
	scale = params[2]

	# define integral
	def integral(x):
			return K*P*x*(sts.gamma.cdf(x, shape, loc, scale)**(P-1))*sts.gamma.pdf(x, shape, loc, scale)

	# gamma defined from min to inf
	expected, error = quad(integral, loc, np.inf)

	return expected


# do everything for some data
def do_the_stuff(data, output_prefix_name, K, P):


	maxi = max(data)

	params1 = fit_gamma_MLE(data)
	params2 = fit_gamma_scipy(data)
	params3 = fit_gamma_moments(data)

	params = [params1, params2, params3]
	names = ["MLE", "scipy", "moments"]
	
	for param, name in zip(params, names):

		print name
		print param

		q_q_plot(data, param, output_prefix_name)
		plot_data_and_gamma(data, param, output_prefix_name, P)

		print calc_expected_runtime(param, K, P)

# get my data for bulk statistics
P = 4096
runtimes = []
for rank in range(P):

	filename = "../ex23/node-64/gmres_rank" + str(rank) + ".txt"
	runtimes.append(get_iter_times(filename))

# get data out of nested list
runtimes = np.array(runtimes[0])

# looks pretty good, except still calculates bad runtime
do_the_stuff(runtimes, "", 5000, 4096)










