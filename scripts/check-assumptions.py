# 
# nice pictures of the assumptions we claimed for the model
# 
# 
# 

import re # for regular expression
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as sts

# read a file for iteration times
def get_iter_times(filename):
	times = []
	file = open(filename, "r")
	for line in file:
		times.append(float(line))
	return times

# read all the files of one run for iteration times
def get_all_iter_times(path_to_data, P, K):

	data = []
	for rank in range(P):

		filename = path_to_data + str(rank) + ".txt"
		data.append(get_iter_times(filename))

	# each entry of data is every iteration from one processor
	data = np.array(data)
	# now make each entry one iteration from every processor
	data_transpose = np.transpose(data)

	return data_transpose

# plot a histogram of runtime from every iteration for a fixed processor
def identical_in_p(path_to_data, path_to_plots):

	# ranks to plot
	rank1, rank2, rank3 = 100, 2048, 2048

	a = get_iter_times(path_to_data + str(rank1) + ".txt")
	b = get_iter_times(path_to_data + str(rank2) + ".txt")
	# c = get_iter_times(path_to_data + str(rank3) + ".txt")

	# parameters
	xmax = 0.001
	n_bins = 75

	# plot
	plt.xlim([0, xmax])
	plt.hist(a, bins=n_bins, normed=True, color="firebrick", fill=False, histtype="step", label="Rank " + str(rank1))
	plt.hist(b, bins=n_bins, normed=True, color="steelblue", fill=False, histtype="step", label="Rank " + str(rank2))
	# plt.hist(c, bins=n_bins, normed=True, color="olivedrab", fill=False, histtype="step", label="Rank " + str(rank3))

	labels = [0, 0.2, 0.4, 0.6, 0.8, 1.0]
	plt.xticks([0, 0.0002, 0.0004, 0.0006, 0.0008, 0.0010], labels)
	plt.title("Krylov iteration density on 4096 processors", fontsize=20)
	plt.xlabel("Milliseconds", fontsize=20)
	plt.ylabel("Density", fontsize=20)
	plt.legend(loc="upper right", fontsize=20)
	plt.savefig(path_to_plots + "identical_in_p.png")
	plt.gcf().clear()
	# plt.show()

# plot a histogram of runtime from every processor for a fixed iteration
def stationary_in_time(path_to_data, P, K, path_to_plots):

	data = get_all_iter_times(path_to_data, P, K)

	# iterations to plot
	iter1, iter2 = 100, 2500

	a = data[iter1]
	b = data[iter2]

	# parameters
	xmin = 0.00015
	xmax = 0.00045
	n_bins = 15

	# plot
	plt.xlim([xmin, xmax])
	plt.hist(a, bins=n_bins, normed=True, color="firebrick", fill=False, histtype="step", label="Iteration " + str(iter1))
	plt.hist(b, bins=n_bins, normed=True, color="steelblue", fill=False, histtype="step", label="Iteration " + str(iter2))

	labels = [0.15, 0.2, 0.3, 0.4, 0.45]
	plt.xticks([0.00015, 0.0002, 0.0003, 0.0004, 0.00045], labels)
	plt.title("Krylov iteration density on 4096 processors", fontsize=20)
	plt.xlabel("Milliseconds", fontsize=20)
	plt.ylabel("Density", fontsize=20)
	plt.legend(loc="upper right", fontsize=20)
	plt.savefig(path_to_plots + "stationary_in_time.png")
	plt.gcf().clear()
	# plt.show()

# run the plots
identical_in_p("../ex23/node-64/gmres_rank", "../ex23/node-64/plots/")
stationary_in_time("../ex23/node-64/gmres_rank", 4096, 5000, "../ex23/node-64/plots/")








