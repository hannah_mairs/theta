# 
# nice pictures
# 
# stochastic model: E[T] = \sum_k E[\max_p T^k_p]
# 	so we need *for a fixed iteration number* the maximum runtime
# 
# 

import re # for regular expression
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as sts

# read a file for iteration times
def get_iter_times(filename):
	times = []
	file = open(filename, "r")
	for line in file:
		times.append(float(line))
	return times

# read all the files of one run for iteration times
def get_all_iter_times():

	P = 4096 # total number of processors
	K = 5000 # total number of iterations

	data = []
	for rank in range(P):

		filename = "../ex23/node-64/gmres_rank" + str(rank) + ".txt"
		data.append(get_iter_times(filename))

	# each entry of data is every iteration from one processor
	data = np.array(data)
	# now make each entry one iteration from every processor
	data_transpose = np.transpose(data)

	return data_transpose

# plot a histogram of runtime from every processor for a fixed iteration
def plot_hist():

	data = get_all_iter_times()

	# iterations to plot
	iter1, iter2 = 100, 2500

	a = data[iter1]
	b = data[iter2]

	# parameters
	# xmax = 0.001
	n_bins = 10

	# plot
	# plt.xlim([0, xmax])
	plt.hist(a, bins=n_bins, normed=True, color="firebrick", fill=False, histtype="step", label="Iteration 100")
	plt.hist(b, bins=n_bins, normed=True, color="steelblue", fill=False, histtype="step", label="Iteration 2500")

	# labels = [0, 0.2, 0.4, 0.6, 0.8, 1.0]
	# plt.xticks([0, 0.0002, 0.0004, 0.0006, 0.0008, 0.0010], labels)
	plt.title("Krylov iteration density on 4096 processors", fontsize=20)
	plt.xlabel("Milliseconds", fontsize=20)
	plt.ylabel("Density", fontsize=20)
	plt.legend(loc="upper right", fontsize=20)
	plt.savefig("/Users/hannahmorgan/research/argonne-givens/theta/scripts/iter_hist.png")
	plt.gcf().clear()
	# plt.show()

plot_hist()








